import request from './http.js'

export default {

	// 查询指定位置的广告信息
	getAdvertList(position = 1) {
		return request({
			url: `/article/api/advert/show/${position}`
		})
	},

	// 查询分类与标签信息
	getCategoryList() {
		return request({
			url: '/article/api/category/label/list'
		})
	},

	// 条件分页查询课程列表 query条件对象{isFree: 0}，current当前页码，size每页显示多少条
	getList(options) {
		return request({
			url: '/course/api/course/search',
			method: 'POST',
			data: options // {isFree: 0, current: current, size: size}
		})
	},


	// 阅读
	getRead() {
		return request({
			url: "/article/api/category/label/list"
		})
	},

	// 阅读数据
	getReadList(options) {
		return request({
			url: '/article/api/article/search',
			method: "POST",
			data: options
		})
	},

	// 详情页数据
	getsCourseNull(datas) {
		return request({
			url: '/course/api/course/null',
			method: 'get',
			data: datas
		})
	},

	getsChapterNull(datas) {
		return request({
			url: '/course/api/chapter/section/list/null',
			method: 'get',
			data: datas
		})
	},

	getsCommentNull(datas) {
		return request({
			url: '/course/api/comment/list/null',
			method: 'get',
			data: datas
		})
	},

	getSsetmealNull(datas) {
		return request({
			url: '/course/api/group/list/null',
			method: 'get',
			data: datas
		})
	},

	getCode(datas) {
		return request({
			url: '/auth/login',
			method: "POST",
			data: datas
		})
	}

}
